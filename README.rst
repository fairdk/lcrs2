Large-scale Computer Reuse Suite (LCRS)
=======================================

**THIS PROJECT IS BEING DEVELOPED IN A NEXT-GEN VERSION**

Go here:

https://gitlab.com/fairdk/lcrs

--------------

For the benefit of people and the planet, let us start re-using, repairing and refurbishing computers instead of throwing them away
-----------------------------------------------------------------------------------------------------------------------------------

.. figure:: https://raw.githubusercontent.com/fairdk/lcrs2/controller/lcrs_screenshot.png
   :alt: LCRS main window

   LCRS main window

README
======

Thank you for downloading Large-scale Computer Re-use Suite (LCRS).

This program is intended for re-using loads of computers for the benefit
of people and the planet. We hope that you find good use of it and that
you report any problems or contribute with code on our website:

http://lcrs.fairdanmark.dk

Installing
==========

Click this link to install from PPA on Ubuntu/Debian:

`LCRS PPA on
Launchpad <https://launchpad.net/~benjaoming/+archive/ubuntu/lcrs>`__

Installing as raw Python package:

::

    git clone https://gitlab.com/fairdk/lcrs2.git
    cd lcrs2
    sudo python setup.py install

Running
=======

You need to run LCRS as a superuser. This is because it runs a
stand-alone DHCP and TFTP server. This also means that you need to
shutdown any similar service occupying those network ports already.

LCRS consists of a "controller" application and a "consumer" application.

The controller is the one you should run on the computer controlling and
monitoring all the computers you wish to re-use.

1. Check that you have installed the required dependencies.
2. Run the command ``lcrs``
3. Check the network preferences and make sure that it fits your own
   setup.
4. Close and start again if you make changes to the settings.

Configuring
===========

At the first run, you may be told of some issues with the DHCP server.
You can resolve those by changing your Preferences.

After reconfiguring, please restart LCRS.

Advanced users should look in ``~/.coresu.cfg``

Building newer kernels
======================

See the ``buildroot/`` directory, its ``README`` and the ``Makefile``
there.

Plugins
=======

To register data from LCRS, you have to write your own plugin. Please
refer to the plugin documentation for this.

Debugging
=========

Check ``/var/log/lcrs.error.log`` and ``/var/log/lcrs.log``.

Releasing
=========

Releasing is done by bumping the version in ``setup.py``and building an sdist with ``python setup.py sdist``

This sdist is then fed to ``uupdate`` using an existing .deb source package to create a new one.

::

    uupdate --no-symlink -v 2.4.1~b1 /code/lcrs2/dist/lcrs-2.4.1b1.tar.gz

After this, use ppa-copy-packages and add relevant dist names.

::

    ppa-copy-packages --owner benjaoming --name lcrs -p lcrs -s trusty -t xenial bionic focal --verbose
