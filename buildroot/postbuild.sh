#!/bin/bash

set -e

# set_lcrs_env is created on-the-fly by `make configs`
source ./set_lcrs_env.sh

cp $GIT_DIR/menu.lst fs/iso9660/

# cp $GIT_DIR/wipe output/target/usr/bin

mkdir -p output/target/usr/share/hwdata

cp $GIT_DIR/pci.ids output/target/usr/share/hwdata/

# chmod +x output/target/usr/bin/wipe

mkdir -p output/target/usr/local
mkdir -p output/target/usr/init.d

# Clean beacuse of *pyc files etc
cd $GIT_DIR/../lcrs/slave && git clean -f -d && cd -

cp -r $GIT_DIR/../lcrs/slave output/target/usr/local/slave

cp $GIT_DIR/inittab output/target/etc/inittab
cp $GIT_DIR/S99LCRS output/target/etc/init.d/S99LCRS

# echo "udhcpc -n eth0" > output/target/etc/init.d/S50dhcp
# echo "python /usr/local/slave/main.py" > output/target/etc/init.d/S99LCRS

chmod +x output/target/etc/init.d/*
