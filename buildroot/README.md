# Workflow for Buildroot

## Background

We are building both a 32 bit and a 64 bit system and kernel. The produced images are then copied to the PXE bootable structure that is served by LCRS.

The Docker volume mounted in `/build` folder contains:

* `current-i386/` - a symlink pointing to an unpacked buildroot directory intended for 32bit
* `current-x86_64` - a symlink pointing to an unpacked buildroot directory intended for 32bit
* `defconfig_lcrs_i386` - the buildroot defconfig for 32 bit
* `defconfig_lcrs_linux_i386` - the buildroot defconfig for 32 bit
* `defconfig_lcrs_x86_64` - the buildroot defconfig for 32 bit
* `defconfig_lcrs_linux_x86_64` - the buildroot defconfig for 32 bit

The `Makefile` can help you with the following.

* `make skeleton root=/path/to/build/volume` - downloads the currently used version of buildroot and unpacks it for 32bit and 64bit builds.
* `make configs` - installs defconfigs in their respective targets
* `make copyconfigs` - copies current defconfigs to git tracked location
* `make copy_outputs` - copies kernel and initial ramdisk (initrd) for target architectures
* `make clean root=/path/to/build/volume` - clean up everything

## The workflow

Firstly, get the latest buildroot source unpacked. You might need to link it to a separate drive with extra space. Each build takes about 9 GB, so 18 GB in total.

Go to `buildroot/`:

## Dockerized build

Install docker and docker-compose (version 1.25+).

You need a lot of disk space for the build volume which is expected to
be found in /build. If you mount it somewhere else in the container, set
the environment variable `root` to that path.

```bash
# Run this every time you change Dockerfile
# This builds the docker image, not buildroot!
docker-compose build

# Build everything (takes hours!)
docker-compose up -v /media/build/lcrs/:/build
```

Build everything, but clean first - **WARNING THIS IS EXPENSIVE**

```bash
# By cleaning buildroot's build artifacts,
# it's guaranteed to take hours :)
docker-compose run -v /media/build/lcrs/:/build buildroot sh -c "cd /lcrs/buildroot && make images-clean && make images"
```

Finally, to copy the outputs to their distributed location, use
the following command on the host system:

```
root=/path/to/docker/volume make copyoutputs
```

After working with the setup, if you want to copy out the buildroot configuration
files for tracking in git, use this command 

```
docker-compose run buildroot sh -c "cd /lcrs/buildroot && make copyconfigs"
```

## 18.04 raw build

This is the former documentation, which is now Dockerized. You can invoke
these commands using the above `docker run` recipe.

```
make skeleton root=/path/to/drive/with/capacity
make configs  # Copies in configuration files
make  # just builds everything
cd current-i386/  # Go to the 32 bit build and change things
make menuconfig  # Buildroot configuration
make linux-menuconfig  # kernel config
make busybox-menuconfig  # busybox config
cd ..  # Go back
cd current-x86_64  # Do the same for the 64 bit kernel
make copyconfigs  # Copy out current configurations
make  # Build everything
```
